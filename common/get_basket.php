<?php
$ajax = true;
$need_auth = false;
$want_menu = false;
require_once('./common/init.php');

if (!isset($_SESSION['basket'])) {
  $_SESSION['basket'] = [];
}
$basket = $_SESSION['basket'];

echo json_encode($basket);
