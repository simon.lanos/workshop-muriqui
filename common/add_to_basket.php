<?php
$ajax = true;
$need_auth = false;
$want_menu = false;
require_once('./common/init.php');

$record_ids = ($_POST['rids'] ?? []);
$act = ($_POST['act'] ?? null);

if (!isset($_SESSION['basket'])) {
    $_SESSION['basket'] = [];
}
$basket = $_SESSION['basket'];

if (!is_array($record_ids)) {
    $record_ids = explode(',', $record_ids);
}

foreach ($record_ids as $record_id) {
    $record_id = (int) $record_id;

    if ($act === 'add') {
        if (!in_array($record_id, $basket, true)) {
            $basket[] = $record_id;
        }
    } elseif ($act === 'rem') {
        if (in_array($record_id, $basket, true)) {
            $basket = array_diff($basket, array($record_id));
        }
    }
}

$_SESSION['basket'] = $basket;
echo json_encode($basket);
