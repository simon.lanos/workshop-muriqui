export default class FilterManager {
  filterForm = null;

  constructor(filterForm) {
    this.filterForm = filterForm;
    this.init();
  }

  init() {
    this.setEvents();
  }

  setEvents() {
    this.filterForm
      .querySelectorAll(".filter-input-range")
      .forEach((rangeInput) =>
        rangeInput.addEventListener("change", (e) => this.onRangeChange(e))
      );
    this.filterForm
      .querySelectorAll(".filter-input-number")
      .forEach((numberInput) =>
        numberInput.addEventListener("change", (e) =>
          this.onInputNumberChange(e)
        )
      );
    this.filterForm
      .querySelectorAll(".reset-filter_value")
      .forEach((button) =>
        button.addEventListener("click", (e) => this.onResetinputValue(e))
      );
  }

  onRangeChange(e) {
    const rangeValue = e.currentTarget.value;
    const inputId = e.currentTarget.getAttribute("data-input-id");
    const numberInput = this.filterForm.querySelector(`#filter_${inputId}`);
    numberInput.value = rangeValue;
  }

  onInputNumberChange(e) {
    const formatedNumberValue = this.formatNumberValue(e.currentTarget);
    e.currentTarget.value = formatedNumberValue;
    const inputId = e.currentTarget.getAttribute("data-input-id");
    const rangeInput = this.filterForm.querySelector(
      `#filter_${inputId}_range`
    );
    rangeInput.value = formatedNumberValue;
  }

  onResetinputValue(e) {
    const inputId = e.currentTarget.getAttribute("data-input-id");
    const input = this.filterForm.querySelector(`#filter_${inputId}`);
    input.value = "";
  }

  formatNumberValue(input) {
    let formatedValue = Math.min(input.value, input.max);
    formatedValue = Math.max(formatedValue, input.min);
    return formatedValue;
  }
}
