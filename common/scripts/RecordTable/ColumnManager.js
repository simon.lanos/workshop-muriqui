export default class ColumnManager {
  switchButtons = null;
  toggleAllColumnSwitch = null;
  recordTable = null;
  hidableTableHeads = null;
  hidableCells = null;

  constructor() {
    this.switchButtons = document
      .querySelector("#tableColumnSelector")
      .querySelectorAll(".column-switch");
    this.toggleAllColumnSwitch = document.querySelector(
      "#toggleAllColumnSwitch"
    );
    this.recordTable = document.querySelector(".record-table");
    this.hidableTableHeads = this.recordTable.querySelectorAll(
      ".hidable-table-head"
    );
    this.hidableCells = this.recordTable.querySelectorAll(".hidable-cell");
    this.init();
  }

  init() {
    this.setEvents();
  }

  setEvents() {
    // Set events for regular switchs
    this.switchButtons.forEach((switchButton) => {
      switchButton.addEventListener("change", (e) =>
        this.onSwitchButtonChange(e)
      );
      switchButton.removeAttribute("disabled");
    });

    // Set event for toggleAllColumn switch
    this.toggleAllColumnSwitch.addEventListener("change", () => {
      this.toggleAllColumns();
    });

    this.toggleAllColumnSwitch.removeAttribute("disabled");
  }

  onSwitchButtonChange(e) {
    const isSwitchChecked = e.currentTarget.checked;
    const columnName = e.currentTarget.getAttribute("data-column-name");
    const columnHead = document.querySelector(`#tableHead${columnName}`);
    const columnCells = this.recordTable.querySelectorAll(
      `.cell-${columnName}`
    );

    if (isSwitchChecked) {
      columnHead.style = "display: table-cell";
      columnCells.forEach((cell) => (cell.style = "display: table-cell"));
    } else {
      columnHead.style = "display: none";
      columnCells.forEach((cell) => (cell.style = "display: none"));
    }

    this.updateToggleAllSwitchState();
  }

  toggleAllColumns() {
    const isSwitchChecked = this.toggleAllColumnSwitch.checked;

    if (isSwitchChecked) {
      this.switchButtons.forEach((switchButton) => {
        switchButton.checked = true;
      });
      this.hidableTableHeads.forEach(
        (tableHead) => (tableHead.style = "display: table-cell")
      );
      this.hidableCells.forEach((cell) => (cell.style = "display: table-cell"));
    } else {
      this.switchButtons.forEach((switchButton) => {
        switchButton.checked = false;
        this.hidableTableHeads.forEach(
          (tableHead) => (tableHead.style = "display: none")
        );
        this.hidableCells.forEach((cell) => (cell.style = "display: none"));
      });
    }
  }

  updateToggleAllSwitchState() {
    const isAllSwichesChecked = Array.from(this.switchButtons.values()).every(
      (switchButton) => switchButton.checked
    );

    if (isAllSwichesChecked) {
      this.toggleAllColumnSwitch.checked = true;
    } else {
      this.toggleAllColumnSwitch.checked = false;
    }
  }
}
