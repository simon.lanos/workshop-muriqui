export default class CustomAudioPlayer {
  audio = null;
  controls = null;
  play = null;
  timerWrapper = null;
  timer = null;
  slider = null;

  constructor(player) {
    this.audio = player.querySelector("audio");
    this.controls = player.querySelector(".controls");
    this.play = player.querySelector(".play");
    this.timerWrapper = player.querySelector(".timer");
    this.timer = player.querySelector(".timer-count");
    this.slider = player.querySelector(".timer .slider");

    this.init();
  }

  get isLoaded() {
    return this.audio.readyState === 4;
  }

  init() {
    this.setEvents();
  }

  setEvents() {
    this.play.addEventListener("click", () => this.playPauseAudio());
    this.audio.addEventListener("timeupdate", () => this.setTime());
    this.audio.addEventListener("ended", () => this.togglePlayButton());
    this.timerWrapper.addEventListener("click", (e) => this.onTimerClicked(e));
  }

  playPauseAudio() {
    if (this.audio.paused) {
      this.audio.play();
    } else {
      this.audio.pause();
    }
    this.togglePlayButton();
  }

  onTimerClicked(e) {
    if (!this.isLoaded) return;

    const boundings = e.currentTarget.getBoundingClientRect();
    const sliderLength = e.x - boundings.x;

    this.slider.style.width = `${sliderLength}px`;
    this.audio.currentTime =
      this.audio.duration * (sliderLength / boundings.width);
  }

  setTime() {
    const minutes = Math.floor(this.audio.currentTime / 60);
    const seconds = Math.floor(this.audio.currentTime - minutes * 60);
    const minuteValue = minutes < 10 ? `0${minutes}` : minutes;
    const secondValue = seconds < 10 ? `0${seconds}` : seconds;
    const sliderLength =
      this.timerWrapper.clientWidth *
      (this.audio.currentTime / this.audio.duration);

    this.timer.textContent = `${minuteValue}:${secondValue}`;
    this.slider.style.width = `${sliderLength}px`;
  }

  togglePlayButton() {
    this.play.querySelector(".fa-pause").classList.toggle("hidden");
    this.play.querySelector(".fa-play").classList.toggle("hidden");
  }
}
