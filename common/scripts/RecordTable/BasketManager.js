function _objectToFormData(object) {
  const formData = new FormData();
  for (const key in object) {
    formData.append(key, object[key]);
  }
  return formData;
}

export default class BasketManager {
  records = null;
  basket = null;
  toggleAllCheckbox = null;
  tableBodyElement = null;

  constructor(records) {
    this.records = records;
    this.toggleAllCheckbox = document.querySelector("#toggleAllRecords");
    this.tableBodyElement = document.querySelector(".record-table__body");

    this.init();
  }

  get isBasketFull() {
    return this.records.every((record) =>
      this.basket.includes(Number(record[0].record_id))
    );
  }

  get rids() {
    return this.records.map((record) => Number(record[0].record_id));
  }

  async init() {
    this.basket = await this.getBasket();
    this.initEvents();
    this.updateCheckBox();
  }

  initEvents() {
    const addRecordButtons =
      this.tableBodyElement.querySelectorAll(".add-to-basket");
    const removeRecordButtons = this.tableBodyElement.querySelectorAll(
      ".remove-from-basket"
    );

    addRecordButtons.forEach((element) =>
      element.addEventListener("click", (e) => this.addToBasket(e))
    );
    removeRecordButtons.forEach((element) =>
      element.addEventListener("click", (e) => this.removeFromBasket(e))
    );
    this.toggleAllCheckbox.addEventListener("click", () => this.toggleBasket());
  }

  async getBasket() {
    const res = await axios.get("get_basket.php");
    return Object.values(res.data);
  }

  async addToBasket(e) {
    const rid = e.currentTarget.getAttribute("data-recordid");
    await this.updateBasket({ rids: rid, act: "add" });
    document.querySelector(`#b_${rid}_add`).style = "display: none";
    document.querySelector(`#b_${rid}_rem`).style = "display: inline";
    this.updateCheckBox();
  }

  async removeFromBasket(e) {
    const rid = e.currentTarget.getAttribute("data-recordid");
    await this.updateBasket({ rids: rid, act: "rem" });
    document.querySelector(`#b_${rid}_rem`).style = "display: none";
    document.querySelector(`#b_${rid}_add`).style = "display: inline";
    this.updateCheckBox();
  }

  async toggleBasket() {
    const act = this.isBasketFull ? "rem" : "add";

    await this.updateBasket({ rids: this.rids, act });
    this.toggleBasketIcons(act);
    this.updateCheckBox();
  }

  async updateBasket(data) {
    const formData = _objectToFormData(data);
    const res = await axios.post("add_to_basket.php", formData);
    this.basket = Object.values(res.data);
  }

  toggleBasketIcons(act) {
    const iconClassToShow =
      act === "rem" ? ".add-to-basket" : ".remove-from-basket";
    const iconClassToHide =
      act === "add" ? ".add-to-basket" : ".remove-from-basket";

    this.tableBodyElement
      .querySelectorAll(iconClassToHide)
      .forEach((element) => (element.style = "display: none"));
    this.tableBodyElement
      .querySelectorAll(iconClassToShow)
      .forEach((element) => (element.style = "display: inline"));
  }

  updateCheckBox() {
    if (this.isBasketFull) {
      this.toggleAllCheckbox.checked = true;
    } else {
      this.toggleAllCheckbox.checked = false;
    }
  }
}
