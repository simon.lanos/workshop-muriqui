import BasketManager from "./BasketManager.js";
import ColumnManager from "./ColumnManager.js";
import CustomAudioPlayer from "./CustomAudioPlayer.js";
import FilterManager from "./FilterManager.js";

const recordTableElement = document.querySelector("#recordTable");
const filterForm = document.querySelector("#filter-form");

if (recordTableElement) {
  const records = Object.values(JSON.parse(recordTableElement.dataset.records));
  const audioElements = recordTableElement.querySelectorAll(
    ".custom-audio-player"
  );

  new SimpleBar(document.querySelector("#recordTableWrapper"));
  audioElements.forEach((player) => new CustomAudioPlayer(player));
  new BasketManager(records);
  new ColumnManager();
} else {
  throw new Error("No element with id #recordTable found in the DOM.");
}

if (filterForm) {
  new FilterManager(filterForm);
}
